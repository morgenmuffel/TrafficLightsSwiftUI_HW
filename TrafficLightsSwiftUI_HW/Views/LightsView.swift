//
//  LightsView.swift
//  TrafficLightsSwiftUI_HW
//
//  Created by Maksim on 11.12.2023.
//

import SwiftUI

struct LightsView: View {
    let color: Color
    let opacity: Double
    var body: some View {
        ZStack {
            Circle()
                .frame(width: 100)
                .foregroundColor(color)
                .opacity(opacity)
                .overlay(Circle().stroke(.white, lineWidth: 1))
                .shadow(color: color, radius: 15)
            
        }
    }
}

struct LightsView_Previews: PreviewProvider {
    static var previews: some View {
        LightsView(color: .red, opacity: 0.5)
    }
}
