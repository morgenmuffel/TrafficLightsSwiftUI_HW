//
//  ContentView.swift
//  TrafficLightsSwiftUI_HW
//
//  Created by Maksim on 11.12.2023.
//

import SwiftUI

struct ContentView: View {
    
    @State private var opacityRed = 0.3
    @State private var opacityYellow = 0.3
    @State private var opacityGreen = 0.3
    
    var body: some View {
        ZStack {
            Color.black.ignoresSafeArea()
            VStack(spacing: 20) {
                LightsView(color: .red, opacity: opacityRed)
                LightsView(color: .yellow, opacity: opacityYellow)
                LightsView(color: .green, opacity: opacityGreen)
                
                Spacer()
                
                Button(action: { changeOpacity() },
                       label: {
                    if opacityRed == 1.0 || opacityYellow == 1.0 || opacityGreen == 1.0 {
                        Text ("NEXT")
                    } else {
                        Text("Start")
                    }
                })
                .frame(width: 150, height: 60)
                .background(Color.blue).cornerRadius(15)
                .font(.system(size: 30, weight: .bold, design: .default))
                .foregroundColor(.white)
                .overlay(RoundedRectangle(cornerRadius: 20).stroke(Color.white, lineWidth: 2))
                .shadow(color: .blue, radius: 9)
            }
            .padding(.top, 30)
            .padding(.bottom, 40)
        }
    }
    
    private func changeOpacity() {
           if opacityRed < 1.0 && opacityYellow < 1.0 && opacityGreen < 1.0 {
               opacityRed = 1.0
           } else if opacityRed == 1.0 {
               opacityRed = 0.3
               opacityYellow = 1.0
           } else if opacityYellow == 1.0 {
               opacityYellow = 0.3
               opacityGreen = 1.0
           } else if opacityGreen == 1.0 {
               opacityGreen = 0.3
               opacityRed = 1.0
           }
       }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
