//
//  TrafficLightsSwiftUI_HWApp.swift
//  TrafficLightsSwiftUI_HW
//
//  Created by Maksim on 11.12.2023.
//

import SwiftUI

@main
struct TrafficLightsSwiftUI_HWApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
